package br.com.lecom;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.lecom.tecnologia.db.DBUtils;
import com.lecom.workflow.robo.face.WebServices;
import com.lecom.workflow.robo.face.WebServicesVO;

import br.com.lecom.atos.servicos.annotation.Execution;
import br.com.lecom.atos.servicos.annotation.RobotModule;
import br.com.lecom.atos.servicos.annotation.Version;


@RobotModule("Roncador_robo_avaliacao")
@Version({ 1, 0, 0 })
public class Main_Robo_avaliacao implements WebServices {

	// logs;
	private final static Logger LOGGER = Logger.getLogger(Main_Robo_avaliacao.class);
	private Writer WRITER = new StringWriter();
	

	// conex�o com o banco;
	private static Connection conLecom = null;

	// map de par�metros do robo;
	private static Map<String, String> roboMap = UtilRobo.getRoboMap();

	// Resultado consulta SQL
	private Map<String, String> mapAvaliador = new HashMap<>();

	@Execution
	public void exec() throws Exception {

		LOGGER.info("--------------------------------------------------------------------------\r\n");
		LOGGER.info("Iniciando execu��o.\r\n");

		try {
			conLecom = DBUtils.getConnection("encrypted");
			conLecom.setAutoCommit(false);

			// ----------------------------Recuperando codigos dos formularios------------

			String codForm_Liberacao = Util
					.obj2String(Util.getContentProperties(true, "UtilRobo.properties", "codForm_Liberacao"));
			String codForm_180 = Util.obj2String(Util.getContentProperties(true, "UtilRobo.properties", "codForm_180"));
			String codForm_Indicadores = Util
					.obj2String(Util.getContentProperties(true, "UtilRobo.properties", "codForm_Indicadores"));
			String codForm_Desempenho = Util
					.obj2String(Util.getContentProperties(true, "UtilRobo.properties", "codForm_Desempenho"));
			// ------------------------------------------------------------------------------

			try (PreparedStatement pst = conLecom.prepareStatement(UtilRobo.getProcesso(codForm_Liberacao, "2"))) {
				try (ResultSet rs = pst.executeQuery()) {
					while (rs.next()) {

						try (PreparedStatement pst2 = conLecom
								.prepareStatement(UtilQueries.getInfoProcesso(rs.getString(1)))) {
							try (ResultSet rs2 = pst2.executeQuery()) {
								if (rs2.next()) {
									LOGGER.debug(rs2.getString(1));

									switch (rs2.getString(1)) {

									// --------------------------------------------------------------------------------
									// -----------------------------------CASE INDICADORES-----------------------------
									// --------------------------------------------------------------------------------

									case "Cria��o dos Indicadores":
										// roboMap.replace("codForm", codForm_Indicadores);
										// UtilRobo.abreProcesso(roboMap, null, null);
										LOGGER.debug("entrou no case?");
										try (PreparedStatement pstEmail = conLecom
												.prepareStatement(UtilQueries.getEmailGestor(rs.getString(1)))) {
											try (ResultSet rsEmail = pstEmail.executeQuery()) {
												while (rsEmail.next()) {

												

													Map<String, Object> emailMap = new HashMap<>();
													emailMap.put("remetente", "bpm@gruporoncador.com.br");
													emailMap.put("destinatario", "caue.oliveira@infive.com.br"); // emailMap.put("destinatario", rsEmail.getString("des_email"));
													emailMap.put("assunto", "Lecom - Notifica��o sobre o cadastro de informa��es");
													emailMap.put("msg", UtilEmail.getTemplate());
													emailMap.put("html", true);

													UtilEmail.enviaEmail(emailMap);
													LOGGER.debug("email enviado : " + rsEmail.getString("des_email"));
													
												}
											}
										}
										break;

									// --------------------------------------------------------------------------------
									// -----------------------------------CASE AVALIA��O 180---------------------------
									// --------------------------------------------------------------------------------

									case "Avalia��o 180�":

										// recupera avaliadores;
										// --------------------------------------------------------------------------------
										LOGGER.debug("entrou na avalia��o 180");
										try (PreparedStatement pst180Avaliadores = conLecom
												.prepareStatement(UtilQueries.getAvaliadores())) {
											try (ResultSet rs180Avaliadores = pst180Avaliadores.executeQuery()) {
												while (rs180Avaliadores.next()) {
													
													String nomeAvaliador = rs180Avaliadores.getString("NOME_AVALIADOR");
													String codAvaliador = rs180Avaliadores.getString("CODIGO_AVALIADOR");
													String loginAvaliador = rs180Avaliadores.getString("LOGIN");

													Map<String, String> campos = new HashMap<>();
													campos.put("NOME_AVALIADOR", nomeAvaliador);
													campos.put("LOGIN", loginAvaliador);

													// recupera avaliados;
													// --------------------------------------------------------------------

													LinkedList<Map<String, Object>> camposGridList = new LinkedList<>();
													try (PreparedStatement pst180Avaliados = conLecom
															.prepareStatement(UtilQueries.getAvaliados(codAvaliador))) {
														try (ResultSet rs180Avaliados = pst180Avaliados
																.executeQuery()) {
															while (rs180Avaliados.next()) {

																Map<String, Object> camposGrid = new HashMap<>();
																camposGrid.put("nomeGrid", "NOTA");
																camposGrid.put("CODIGO_COLABORADOR",rs180Avaliados.getString("COD_COLABORADOR"));
																camposGrid.put("NOME_COLABORADOR2",rs180Avaliados.getString("NOME_COLABORADOR"));
																
																LOGGER.debug("camposGrid :" + camposGrid);
																LOGGER.debug("rsCod_colaborador :" + rs180Avaliados.getString("COD_COLABORADOR"));
																LOGGER.debug("rsNome_colaborador :" + rs180Avaliados.getString("NOME_COLABORADOR"));
																camposGridList.add(camposGrid);
																
																
															}
														}
													}

													// abre processo;
													// --------------------------------------------------------------------

													LOGGER.debug("campos : " + campos);
													LOGGER.debug("-----------------------------------------");
													LOGGER.debug("camposGridList : " + camposGridList);

													if (camposGridList.size() > 0) {
														
														roboMap.replace("codForm" , codForm_180);
														UtilRobo.abreProcesso(roboMap, campos, camposGridList);
														
													} else {
														LOGGER.info(camposGridList.size());
													}
												}
											}
										}
										break;

									// --------------------------------------------------------------------------------
									// -------------------------------CASE AVALIA��O DE DESEMPENHO---------------------
									// --------------------------------------------------------------------------------

									case "Avalia��o de Desempenho":

										try (PreparedStatement pstDadosColaborador = conLecom
												.prepareStatement(UtilQueries.getDadosColaborador())) {
											try (ResultSet rsDadosColaborador = pstDadosColaborador.executeQuery()) {
												while (rsDadosColaborador.next()) {
													String unidadeFuncional = rsDadosColaborador.getString("UNIDADE_FUNCIONAL");
													String nomeColaborador = rsDadosColaborador.getString("NOME_COLABORADOR");
													String cargoFuncao = rsDadosColaborador.getString("CARGO_FUNCAO");
													String totalRv = rsDadosColaborador.getString("TOTAL_RV_CARGO");
													String salarioAtual = rsDadosColaborador.getString("SALARIO_ATUAL");
													String mesesTrabalhados = rsDadosColaborador.getString("MESES_TRABALHADO");

													Map<String, String> camposColaborador = new HashMap<>();
													camposColaborador.put("UNI_FUNCIONAL", unidadeFuncional);
													camposColaborador.put("NOME_COLABORADOR", nomeColaborador);
													camposColaborador.put("CARGO_FUNCAO", cargoFuncao);
													camposColaborador.put("TOTAL_RV_CARGO", totalRv);
													camposColaborador.put("SALARIO_ATUAL", salarioAtual);
													camposColaborador.put("MESES", mesesTrabalhados);
													
													// recupera indicadores;
													// --------------------------------------------------------------------

													LinkedList<Map<String, Object>> camposGridList_Ind = new LinkedList<>();
													try (PreparedStatement pstIndicadores = conLecom
															.prepareStatement(UtilQueries.getIndicadores(nomeColaborador))) {
														try (ResultSet rsIndicadores = pstIndicadores
																.executeQuery()) {
															while (rsIndicadores.next()) {
																
																String tipo_ind = rsIndicadores.getString("TIPO_INDICADOR");
																
																
																LOGGER.debug("tipo_ind : " + tipo_ind);
																switch (tipo_ind) {
																
																case "INDICADORES DE COMPET�NCIA":
																	
																	Map<String, Object> camposGrid_Competencia = new HashMap<>();
																	camposGrid_Competencia.put("nomeGrid", "INDICADOR");
																	camposGrid_Competencia.put("NOME_INDICADOR",rsIndicadores.getString("NOME_INDICADOR"));
																	camposGrid_Competencia.put("DESCRICAO_INDICADOR",rsIndicadores.getString("DESCRICAO_INDICADOR"));
																	camposGrid_Competencia.put("TIPO_INDICADOR",rsIndicadores.getString("TIPO_INDICADOR"));
																	camposGrid_Competencia.put("PESO",rsIndicadores.getString("PESO"));
																	camposGrid_Competencia.put("FIRMADO",rsIndicadores.getString("FIRMADO"));
																	camposGridList_Ind.add(camposGrid_Competencia);
																	
																	LOGGER.debug("camposQuali : " + camposGrid_Competencia);

																	break;

																case "INDICADORES QUALITATIVOS E QUANTITATIVOS":
																	Map<String, Object> camposGrid_Qualitativo = new HashMap<>();
																	camposGrid_Qualitativo.put("nomeGrid", "IND_QUALI");
																	camposGrid_Qualitativo.put("NOME_IQ",rsIndicadores.getString("NOME_INDICADOR"));
																	camposGrid_Qualitativo.put("DESCRICAO_IQ",rsIndicadores.getString("DESCRICAO_INDICADOR"));
																	camposGrid_Qualitativo.put("TIPO_IQ",rsIndicadores.getString("TIPO_INDICADOR"));
																	camposGrid_Qualitativo.put("PESO_IQ",rsIndicadores.getString("PESO"));
																	camposGrid_Qualitativo.put("FIRMADO_IQ",rsIndicadores.getString("FIRMADO"));
																	camposGridList_Ind.add(camposGrid_Qualitativo);
																
																	LOGGER.debug("camposQuali : " + camposGrid_Qualitativo);
																	break;
																	
																case "INDICADORES DE DESEMPENHO DO NEG�CIO":
																	Map<String, Object> camposGrid_Negocio = new HashMap<>();
																	camposGrid_Negocio.put("nomeGrid", "IND_NEGOCIO");
																	camposGrid_Negocio.put("NOME_IN",rsIndicadores.getString("NOME_INDICADOR"));
																	camposGrid_Negocio.put("DESCRICAO_IN",rsIndicadores.getString("DESCRICAO_INDICADOR"));
																	camposGrid_Negocio.put("TIPO_IN",rsIndicadores.getString("TIPO_INDICADOR"));
																	camposGrid_Negocio.put("PESO_IN",rsIndicadores.getString("PESO"));
																	camposGrid_Negocio.put("FIRMADO_IN",rsIndicadores.getString("FIRMADO"));
																	camposGridList_Ind.add(camposGrid_Negocio);
																	
																	LOGGER.debug("camposNegocio : " + camposGrid_Negocio);
																	break;

																default:
																	break;
																}

															}
														}
													}
													
													roboMap.replace("codForm" , codForm_Desempenho);
													LOGGER.debug("camposColaborador : " + camposColaborador);
													
													UtilRobo.abreProcesso(roboMap, camposColaborador, camposGridList_Ind);
													
													
													
												}
											}
										}

										//Lembrar de finalizar o processo de libera��o de atividades ao final do robo\\
										roboMap.replace("codForm", codForm_Liberacao);
										roboMap.replace("codProcesso", rs.getString("COD_PROCESSO"));
										roboMap.replace("codEtapa", "2");
										roboMap.replace("codCiclo", "1");
										
										LOGGER.debug("roboMap : " + roboMap);

										UtilRobo.executaEtapa(roboMap);
									}
								}
							}
						

						
						}

					}
				}
			}
			
			
			LOGGER.info("Finalizando execu��o.\r\n");

		} catch (Exception e) {
			Util.manageConnection(conLecom, "rollback");
			e.printStackTrace(new PrintWriter(WRITER));
			String error = WRITER.toString();
			LOGGER.error("ERRO: " + error + "\r\n");

		} finally {
			Util.manageConnection(conLecom, "close");
		}
	}

	@Override
	public List<WebServicesVO> getWebServices() {
		return null;
	}

	@Override
	public void setWebServices(WebServicesVO arg0) {
	}

}
