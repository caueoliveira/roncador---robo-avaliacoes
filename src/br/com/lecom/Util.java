package br.com.lecom;

import static br.com.lecom.api.factory.ECMFactory.documento;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Util {

//  codigo do processo no template de email do lecom:
//	String codProcessoConvertido = Util.fillSpaces(codProcesso, 6, "0", true);
//	codProcessoConvertido = codProcessoConvertido.substring(0, 3) + "."
//			+ codProcessoConvertido.substring(3, 6);

	/**
	 * 
	 * @param arg      valor;
	 * @param qtde     quantidade de caracteres necessarios;
	 * @param key      caracter que sera utilizado no preenchimento;
	 * @param trailing inserir caracter(es) no comeco da string;
	 * @return string com espacos preenchidos;
	 */
	public static String fillSpaces(String arg, Integer qtde, String key, Boolean trailing) {
		StringBuilder strb = new StringBuilder();
		for (int i = 0; i < (qtde - arg.length()); i++) {
			strb.append(key);
		}
		String str = strb.toString();
		return trailing ? (str + arg) : (arg + str);
	}

//	String fileDir = codProcesso + codEtapa + codCiclo;
//	InputStream fileContent = Util.getAnexoLecom(form, "FRM_ARQ_PDF");
//	String fileName = Util.getAnexoNomeLecom(form, "FRM_ARQ_PDF");
//	File filePath = Util.generateFileLecom(fileDir, fileContent, fileName);

	/**
	 * 
	 * @param fileDir     diretorio do arquivo;
	 * @param fileContent conteudo do arquivo;
	 * @param fileName    nome do arquivo;
	 * @return arquivo gerado no servidor;
	 * @throws Exception
	 */
	public static File generateFile(String fileDir, InputStream fileContent, String fileName) throws Exception {
		File dir = new File("/opt/lecom/misc/anexos/" + fileDir);
		dir.mkdirs();
		File filePath = new File(dir.getPath() + "/" + fileName);
		Files.copy(fileContent, filePath.toPath(), StandardCopyOption.REPLACE_EXISTING);
		return filePath;
	}

	/**
	 * 
	 * @param form    map de campos do formulario;
	 * @param campoId id do campo anexo;
	 * @return conteudo do anexo;
	 * @throws Exception
	 */
	public static InputStream getAnexo(Map<String, Object> form, String campoId) throws Exception {
		return documento().lerArquivo(Util.obj2String(form.get("$" + campoId)).split(":")[1]);
	}

	/**
	 * 
	 * @param form    map de campos do formulario;
	 * @param campoId id do campo anexo;
	 * @return nome do anexo;
	 */
	public static String getAnexoNome(Map<String, Object> form, String campoId) {
		return Util.obj2String(form.get("$" + campoId)).split(":")[0];
	}

	/**
	 * 
	 * @param readFromClasspath ler arquivo do classpath;
	 * @param filePath          caminho completo do arquivo;
	 * @param key               nome da chave;
	 * @return valor da chave no arquivo.properties;
	 * @throws Exception
	 */
	public static String getContentProperties(Boolean readFromClasspath, String filePath, String key) throws Exception {
		Properties prop = new Properties();
		prop.load(readFromClasspath ? Util.class.getResourceAsStream(filePath) : new FileInputStream(filePath));
		return prop.getProperty(key);
	}

	/**
	 * 
	 * @return data e hora corrente;
	 */
	public static ZonedDateTime getCurrentDateTime() {
		ZoneId fusoHorarioSP = ZoneId.of("America/Sao_Paulo");
		return ZonedDateTime.now(fusoHorarioSP);
	}

	/**
	 * 
	 * @return dia corrente;
	 */
	public static DayOfWeek getCurrentDay() {
		return getCurrentDateTime().getDayOfWeek();
	}
	
	/**
	 * 
	 * @return dia do m�s corrente;
	 */
	public static int CurrentDayOfMonth() {
		LocalDateTime now = LocalDateTime.now();
		int DayOfMonth = now.getDayOfMonth();
		return DayOfMonth;
	}


	/**
	 * 
	 * @param arg valor;
	 * @return string formatada para inser��o no banco de dados;
	 */
	public static String getDbValueString(String arg) {
		return "'" + (arg != null ? arg.trim().replaceAll("\\\"|\\'", "") : "") + "'";
	}

	/**
	 * 
	 * @param connection  conex�o com o banco de dados;
	 * @param codProcesso c�digo do processo;
	 * @return booleano do processo em modo teste;
	 * @throws Exception
	 */
	public static Boolean getModoTeste(Connection connection, String codProcesso) throws Exception {
		Boolean modoTeste = null;
		PreparedStatement pst = connection
				.prepareStatement("SELECT IDE_BETA_TESTE FROM processo WHERE cod_processo = " + codProcesso);
		ResultSet rs = pst.executeQuery();
		if (rs.next())
			modoTeste = rs.getString("IDE_BETA_TESTE").equals("S") ? true : false;
		return modoTeste;
	}

	/**
	 * 
	 * @param error
	 * @param response
	 * @return map com as chaves 'error' e 'response';
	 */
	public static Map<String, Object> getResponseMap(Boolean error, Object response) {
		Map<String, Object> map = new HashMap<>();
		map.put("error", error);
		map.put("response", response);
		return map;
	}

//  select;
//  try (PreparedStatement pst = conLecom.prepareStatement("QUERY")) {
//  try (ResultSet rs = pst.executeQuery()) {
//  while (rs.next()) {
//  }
//  }
//  }

//  update/ insert;
//  try (PreparedStatement pst = conLecom.prepareStatement("QUERY")) {
//  pst.executeUpdate();
//  }
//	conLecom.commit();

	/**
	 * gerenciador de conex�o com o banco de dados;
	 * 
	 * @param connection conex�o com o banco de dados;
	 * @param action     'close' | 'rollback'
	 * @throws Exception
	 */
	public static void manageConnection(Connection connection, String action) throws Exception {
		if (connection != null) {
			switch (action) {
			case "close":
				connection.close();
				break;
			case "rollback":
				connection.rollback();
				break;
			default:
				break;
			}
		}
	}

	/**
	 * 
	 * @param arg Object
	 * @return objeto convertido em Boolean;
	 */
	public static Boolean obj2Boolean(Object arg) {
		return arg != null ? Boolean.valueOf(obj2String(arg)) : null;
	}

	/**
	 * 
	 * @param arg Object
	 * @return objeto convertido em BigDecimal;
	 */
	public static BigDecimal obj2Decimal(Object arg) {
		return arg != null ? new BigDecimal(obj2String(arg).replace(",", ".")) : null;
	}

	/**
	 * 
	 * @param arg Object
	 * @return objeto convertido em Integer;
	 */
	public static Integer obj2Integer(Object arg) {
		return arg != null ? Integer.valueOf(obj2String(arg)) : null;
	}

	/**
	 * 
	 * @param arg Object
	 * @return objeto convertido em String;
	 */
	public static String obj2String(Object arg) {
		return arg != null ? String.valueOf(arg).replaceAll("\\null", "") : "";
	}

	/**
	 * 
	 * @param arg            Date
	 * @param invertedOutput retornar no padr�o: yyyyMMdd
	 * @return objeto convertido em Date (String);
	 */
	public static String obj2StringDate(Date arg, Boolean invertedOutput) {
		return new SimpleDateFormat(invertedOutput ? "yyyy-MM-dd" : "dd-MM-yyyy").format(arg);
	}

	/**
	 * 
	 * @param arg            Object
	 * @param invertedInput  receber no padr�o: yyyyMMdd
	 * @param invertedOutput retornar no padr�o: yyyyMMdd
	 * @return objeto convertido em Date (String);
	 */
	public static String obj2StringDate(Object arg, Boolean invertedInput, Boolean invertedOutput) {
		String output = null;
		String date = obj2String(arg);
		if (date.length() >= 10) {
			if (!invertedInput) { // dd-MM-yyyy
				if (invertedOutput) { // yyyy-MM-dd
					output = (date.substring(6, 10)) + "/" + (date.substring(3, 5)) + "/" + (date.substring(0, 2));
				} else {
					output = date.replace("-", "/");
				}
			} else { // yyyy-MM-dd
				if (!invertedOutput) { // dd-MM-yyyy
					output = (date.substring(8, 10)) + "/" + (date.substring(5, 7)) + "/" + (date.substring(0, 4));
				} else {
					output = date.replace("-", "/");
				}
			}
		}
		return output != null ? output.substring(0, 10) : null;
	}

	/**
	 * 
	 * @param arg           Object
	 * @param invertedInput receber no padr�o: yyyyMMdd
	 * @param time          hhMMss
	 * @return objeto convertido em ZonedDateTime;
	 */
	public static ZonedDateTime obj2ZonedDateTime(Object arg, Boolean invertedInput, String time) {
		return ZonedDateTime
				.parse((Util.obj2StringDate(arg, invertedInput, true).replace("/", "-") + ("T" + time + "Z")));
	}

}

// debug;
// org.apache.log4j.BasicConfigurator.configure();