package br.com.lecom;

import java.util.List;
import java.util.Map;

import com.lecom.workflow.robo.satelite.WFMail;

import br.com.lecom.workflow.email.EmailMessage;

@SuppressWarnings("unused")
public class UtilEmail {

	/**
	 * 
	 * @return template de email;
	 */
	public static String getTemplate() {
		StringBuilder strb = new StringBuilder();

		strb.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n" + 
				"<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n" + 
				"  <head>\r\n" + 
				"    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n" + 
				"    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\r\n" + 
				"    <style type=\"text/css\">\r\n" + 
				"      @import url(http://fonts.googleapis.com/css?family=Lato:400);\r\n" + 
				"      img {\r\n" + 
				"        max-width: 600px;\r\n" + 
				"        outline: none;\r\n" + 
				"        text-decoration: none;\r\n" + 
				"        -ms-interpolation-mode: bicubic;\r\n" + 
				"      }\r\n" + 
				"      a {\r\n" + 
				"        text-decoration: none;\r\n" + 
				"        border: 0;\r\n" + 
				"        outline: none;\r\n" + 
				"      }\r\n" + 
				"      a img {\r\n" + 
				"        border: none;\r\n" + 
				"      }\r\n" + 
				"      /* General styling */\r\n" + 
				"      td,\r\n" + 
				"      h1,\r\n" + 
				"      h2,\r\n" + 
				"      h3 {\r\n" + 
				"        font-family: Helvetica, Arial, sans-serif;\r\n" + 
				"        font-weight: 400;\r\n" + 
				"      }\r\n" + 
				"      body {\r\n" + 
				"        -webkit-font-smoothing: antialiased;\r\n" + 
				"        -webkit-text-size-adjust: none;\r\n" + 
				"        width: 100%;\r\n" + 
				"        height: 100%;\r\n" + 
				"        color: #37302d;\r\n" + 
				"        background: #ffffff;\r\n" + 
				"      }\r\n" + 
				"      table {\r\n" + 
				"        border-collapse: collapse !important;\r\n" + 
				"      }\r\n" + 
				"      h1,\r\n" + 
				"      h2,\r\n" + 
				"      h3 {\r\n" + 
				"        padding: 0;\r\n" + 
				"        margin: 0;\r\n" + 
				"        color: #ffffff;\r\n" + 
				"        font-weight: 400;\r\n" + 
				"      }\r\n" + 
				"      h3 {\r\n" + 
				"        color: #21c5ba;\r\n" + 
				"        font-size: 24px;\r\n" + 
				"      }\r\n" + 
				"      .important-font {\r\n" + 
				"        color: #21beb4;\r\n" + 
				"        font-weight: bold;\r\n" + 
				"      }\r\n" + 
				"      .hide {\r\n" + 
				"        display: none !important;\r\n" + 
				"      }\r\n" + 
				"      .force-full-width {\r\n" + 
				"        width: 100% !important;\r\n" + 
				"      }\r\n" + 
				"    </style>\r\n" + 
				"    <style type=\"text/css\" media=\"screen\">\r\n" + 
				"      @media screen {\r\n" + 
				"        td,\r\n" + 
				"        h1,\r\n" + 
				"        h2,\r\n" + 
				"        h3 {\r\n" + 
				"          font-family: 'Lato', 'Helvetica Neue', 'Arial', 'sans-serif' !important;\r\n" + 
				"        }\r\n" + 
				"      }\r\n" + 
				"    </style>\r\n" + 
				"    <style type=\"text/css\" media=\"only screen and (max-width: 480px)\">\r\n" + 
				"      /* Mobile styles */\r\n" + 
				"      @media only screen and (max-width: 480px) {\r\n" + 
				"        table[class='w320'] {\r\n" + 
				"          width: 320px !important;\r\n" + 
				"        }\r\n" + 
				"        table[class='w300'] {\r\n" + 
				"          width: 300px !important;\r\n" + 
				"        }\r\n" + 
				"        table[class='w290'] {\r\n" + 
				"          width: 290px !important;\r\n" + 
				"        }\r\n" + 
				"        td[class='w320'] {\r\n" + 
				"          width: 320px !important;\r\n" + 
				"        }\r\n" + 
				"        td[class='mobile-center'] {\r\n" + 
				"          text-align: center !important;\r\n" + 
				"        }\r\n" + 
				"        td[class*='mobile-padding'] {\r\n" + 
				"          padding-left: 20px !important;\r\n" + 
				"          padding-right: 20px !important;\r\n" + 
				"          padding-bottom: 20px !important;\r\n" + 
				"        }\r\n" + 
				"        td[class*='mobile-block'] {\r\n" + 
				"          display: block !important;\r\n" + 
				"          width: 100% !important;\r\n" + 
				"          text-align: left !important;\r\n" + 
				"          padding-bottom: 20px !important;\r\n" + 
				"        }\r\n" + 
				"        td[class*='mobile-border'] {\r\n" + 
				"          border: 0 !important;\r\n" + 
				"        }\r\n" + 
				"        td[class*='reveal'] {\r\n" + 
				"          display: block !important;\r\n" + 
				"        }\r\n" + 
				"      }\r\n" + 
				"    </style>\r\n" + 
				"  </head>\r\n" + 
				"  <body\r\n" + 
				"    class=\"body\"\r\n" + 
				"    style=\"padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none\"\r\n" + 
				"    bgcolor=\"#ffffff\"\r\n" + 
				"  >\r\n" + 
				"    <table\r\n" + 
				"      align=\"center\"\r\n" + 
				"      cellpadding=\"0\"\r\n" + 
				"      cellspacing=\"0\"\r\n" + 
				"      width=\"100%\"\r\n" + 
				"      height=\"100%\"\r\n" + 
				"    >\r\n" + 
				"      <tr>\r\n" + 
				"        <td align=\"center\" valign=\"top\" bgcolor=\"#ffffff\" width=\"100%\">\r\n" + 
				"          <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\r\n" + 
				"            <tr>\r\n" + 
				"              <td style=\"border-bottom: 3px solid #3bcdc3;\" width=\"100%\">\r\n" + 
				"                <center>\r\n" + 
				"                  <table\r\n" + 
				"                    cellspacing=\"0\"\r\n" + 
				"                    cellpadding=\"0\"\r\n" + 
				"                    width=\"500\"\r\n" + 
				"                    class=\"w320\"\r\n" + 
				"                  >\r\n" + 
				"                    <tr>\r\n" + 
				"                      <td\r\n" + 
				"                        valign=\"top\"\r\n" + 
				"                        style=\"padding:10px 0; text-align:left;\"\r\n" + 
				"                        class=\"mobile-center\"\r\n" + 
				"                      >\r\n" + 
				"                        <!-- empresaLogo -->\r\n" + 
				"                        <!-- ---------------------------------------------------------------------- -->\r\n" + 
				"                        <img\r\n" + 
				"                          src=\"http://intranet.roncador.com.br/Citrix/XenApp/media/CitrixXenApp.png\"\r\n" + 
				"                        />\r\n" + 
				"                      </td>\r\n" + 
				"                    </tr>\r\n" + 
				"                  </table>\r\n" + 
				"                </center>\r\n" + 
				"              </td>\r\n" + 
				"            </tr>\r\n" + 
				"            <tr>\r\n" + 
				"              <td\r\n" + 
				"                background=\"https://www.filepicker.io/api/file/kmlo6MonRpWsVuuM47EG\"\r\n" + 
				"                bgcolor=\"#8b8284\"\r\n" + 
				"                valign=\"top\"\r\n" + 
				"                style=\"background: url(https://www.filepicker.io/api/file/kmlo6MonRpWsVuuM47EG) no-repeat center; background-color: #8b8284; background-position: center;\"\r\n" + 
				"              >\r\n" + 
				"                <div>\r\n" + 
				"                  <center>\r\n" + 
				"                    <table\r\n" + 
				"                      cellspacing=\"0\"\r\n" + 
				"                      cellpadding=\"0\"\r\n" + 
				"                      width=\"530\"\r\n" + 
				"                      height=\"303\"\r\n" + 
				"                      class=\"w320\"\r\n" + 
				"                    >\r\n" + 
				"                      <tr>\r\n" + 
				"                        <td\r\n" + 
				"                          valign=\"middle\"\r\n" + 
				"                          style=\"vertical-align:middle; padding-right: 15px; padding-left: 15px; text-align:left;\"\r\n" + 
				"                          height=\"303\"\r\n" + 
				"                        >\r\n" + 
				"                          <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\r\n" + 
				"                            <tr>\r\n" + 
				"                              <td>\r\n" + 
				"                                <h1>Cadastro de Informa��es</h1>\r\n" + 
				"                                <br />\r\n" + 
				"                                <h2>\r\n" + 
				"                                  Para realizarmos a Avalia��o de Desempenho, � necess�rio que sejam cadastrados os colaboradores e tamb�m suas metas(Indicadores)\r\n" + 
				"                                  Fique atento aos prazos e siga os passos na ordem:\r\n" + 
				"                                </h2>\r\n" + 
				"                                <br />\r\n" + 
				"                              </td>\r\n" + 
				"                            </tr>\r\n" + 
				"                          </table>\r\n" + 
				"\r\n" + 
				"                          <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\r\n" + 
				"                            <tr>\r\n" + 
				"                              <td class=\"hide reveal\">\r\n" + 
				"                                &nbsp;\r\n" + 
				"                              </td>\r\n" + 
				"                              <td\r\n" + 
				"                                style=\"width:150px; height:33px; background-color: #3bcdc3;\"\r\n" + 
				"                              >\r\n" + 
				"                                <div>\r\n" + 
				"                                  <a\r\n" + 
				"                                    href=\"\"\r\n" + 
				"                                    style=\"background-color:#3bcdc3;border-radius:4px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:150px;-webkit-text-size-adjust:none;\"\r\n" + 
				"                                    >Acessar</a\r\n" + 
				"                                  >\r\n" + 
				"                                </div>\r\n" + 
				"                              </td>\r\n" + 
				"                              <td>\r\n" + 
				"                                &nbsp;\r\n" + 
				"                              </td>\r\n" + 
				"                            </tr>\r\n" + 
				"                          </table>\r\n" + 
				"                        </td>\r\n" + 
				"                      </tr>\r\n" + 
				"                    </table>\r\n" + 
				"                  </center>\r\n" + 
				"                </div>\r\n" + 
				"              </td>\r\n" + 
				"            </tr>\r\n" + 
				"            <tr class=\"force-full-width\">\r\n" + 
				"              <td valign=\"top\" class=\"force-full-width\">\r\n" + 
				"                <center>\r\n" + 
				"                  <table\r\n" + 
				"                    cellspacing=\"0\"\r\n" + 
				"                    cellpadding=\"0\"\r\n" + 
				"                    width=\"500\"\r\n" + 
				"                    class=\"w320\"\r\n" + 
				"                  >\r\n" + 
				"                    <!-- content -->\r\n" + 
				"                    <!-- -------------------------------------------------------------------------- -->\r\n" + 
				"                    <tr>\r\n" + 
				"                      <td valign=\"top\" style=\"border-bottom:1px solid #a1a1a1;\">\r\n" + 
				"                        <table\r\n" + 
				"                          cellspacing=\"0\"\r\n" + 
				"                          cellpadding=\"0\"\r\n" + 
				"                          class=\"force-full-width\"\r\n" + 
				"                        >\r\n" + 
				"                          <tr>\r\n" + 
				"                            <td style=\"padding: 30px 0;\" class=\"mobile-padding\">\r\n" + 
				"                              <table\r\n" + 
				"                                class=\"force-full-width\"\r\n" + 
				"                                cellspacing=\"0\"\r\n" + 
				"                                cellpadding=\"0\"\r\n" + 
				"                              >\r\n" + 
				"                               \r\n" + 
				"                              </table>\r\n" + 
				"                            </td>\r\n" + 
				"                          </tr>\r\n" + 
				"                          <tr>\r\n" + 
				"                            <td\r\n" + 
				"                              style=\"padding-bottom: 30px;\"\r\n" + 
				"                              class=\"mobile-padding\"\r\n" + 
				"                            >\r\n" + 
				"                              <table\r\n" + 
				"                                class=\"force-full-width\"\r\n" + 
				"                                cellspacing=\"0\"\r\n" + 
				"                                cellpadding=\"0\"\r\n" + 
				"                              >\r\n" + 
				"                                <tr>\r\n" + 
				"                                  <td class=\"mobile-block\">\r\n" + 
				"                                    <table\r\n" + 
				"                                      cellspacing=\"0\"\r\n" + 
				"                                      cellpadding=\"0\"\r\n" + 
				"                                      class=\"force-full-width\"\r\n" + 
				"                                    >\r\n" + 
				"                                      <tr>\r\n" + 
				"                                        <td\r\n" + 
				"                                          class=\"mobile-border\"\r\n" + 
				"                                          style=\"background-color: #3bcdc3; color: #ffffff; padding: 5px; border-right: 0px solid #ffffff; text-align:left;\"\r\n" + 
				"                                        >\r\n" + 
				"                                          Aguardando Cadastro\r\n" + 
				"                                        </td>\r\n" + 
				"                                      </tr>\r\n" + 
				"                                      <tr>\r\n" + 
				"                                        <td\r\n" + 
				"                                          style=\"background-color: #ebebeb; padding: 8px; border-top: 3px solid #ffffff; text-align:left;\"\r\n" + 
				"                                        >\r\n" + 
				"                                          1 - Cadastro do Colaborador\r\n" + 
				"                                          De 01/01 at� 02/02\r\n" + 
				"                                          <br />\r\n" + 
				"                                          <br />\r\n" + 
				"                                          2 - Cadastro dos Indicadores\r\n" + 
				"                                          De 03/03 at� 04/04\r\n" + 
				"                                        </td>\r\n" + 
				"                                      </tr>\r\n" + 
				"                                    </table>\r\n" + 
				"                                  </td>\r\n" + 
				"                                </tr>\r\n" + 
				"                              </table>\r\n" + 
				"                            </td>\r\n" + 
				"                          </tr>\r\n" + 
				"                        </table>\r\n" + 
				"                      </td>\r\n" + 
				"                    </tr>\r\n" + 
				"                    <!-- -------------------------------------------------------------------------- -->\r\n" + 
				"                    <tr>\r\n" + 
				"                      <td>\r\n" + 
				"                        <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\r\n" + 
				"                          <tr>\r\n" + 
				"                            <td class=\"mobile-padding\" style=\"text-align:left;\">\r\n" + 
				"                              <br />\r\n" + 
				"                              D�vidas ou solicita��es? Entre em contato pelo\r\n" + 
				"                              endere�o\r\n" + 
				"                              <a style=\"color: #2BB6AC;\" href=\"#\"\r\n" + 
				"                                >suporte@infive.com.br</a\r\n" + 
				"                              >\r\n" + 
				"                              <br />\r\n" + 
				"                              <br />\r\n" + 
				"                              inFive Sistemas de Informa��o\r\n" + 
				"                              <br />\r\n" + 
				"                              <br />\r\n" + 
				"                              <br />\r\n" + 
				"                            </td>\r\n" + 
				"                          </tr>\r\n" + 
				"                        </table>\r\n" + 
				"                      </td>\r\n" + 
				"                    </tr>\r\n" + 
				"                  </table>\r\n" + 
				"                </center>\r\n" + 
				"              </td>\r\n" + 
				"            </tr>\r\n" + 
				"          </table>\r\n" + 
				"        </td>\r\n" + 
				"      </tr>\r\n" + 
				"    </table>\r\n" + 
				"  </body>\r\n" + 
				"</html>\r\n" + 
				"");

		return strb.toString();
	}

//  snipp - emailMap:
//	Map<String, Object> emailMap = new HashMap<>();
//	emailMap.put("remetente", null);
//	emailMap.put("destinatario", null);
//	emailMap.put("assunto", null);
//	emailMap.put("msg", null);
//	emailMap.put("html", true);

	/**
	 * envia email;
	 * 
	 * @param emailMap mas com as chaves do 'emailMap';
	 * @throws Exception
	 */
	public static void enviaEmail(Map<String, Object> emailMap) throws Exception {
		EmailMessage email = new EmailMessage(Util.obj2String(emailMap.get("assunto")),
				Util.obj2String(emailMap.get("msg")), Util.obj2String(emailMap.get("remetente")),
				Util.obj2String(emailMap.get("destinatario")), Util.obj2Boolean(emailMap.get("html")));
		new WFMail().enviaEmailMessage(email);
	}

}
