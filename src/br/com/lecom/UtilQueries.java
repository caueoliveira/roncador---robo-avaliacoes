package br.com.lecom;



public class UtilQueries {
	// --------------------------------------------------------------------------------
	//-----------------------------Queries Avalia��o 180---------------------------

	public static String getAvaliadores() {
		StringBuilder strb = new StringBuilder();
		strb.append(" SELECT");
		strb.append(" NOME_AVALIADOR,");
		strb.append(" CODIGO_AVALIADOR,");
		strb.append(" LOGIN");
		strb.append(" FROM avaliador_180");
		strb.append(" ORDER BY nome_avaliador");
		return strb.toString();
	}
	
	public static String getAvaliados(String codAvaliador) {
		StringBuilder strb = new StringBuilder();
		strb.append(" SELECT");
		strb.append(" cc.NOME_COLABORADOR,");
		strb.append(" cc.COD_COLABORADOR");
		strb.append(" FROM avaliacao_180 a180");
		strb.append(" LEFT JOIN cadastro_colaborador cc ON cc.cod_colaborador = a180.codigo_colaborador");
		strb.append(" WHERE a180.codigo_avaliador = " + codAvaliador);
		return strb.toString();
	}
	
	// --------------------------------------------------------------------------------
	//-----------------------------Queries Avalia��o Desempenho---------------------------
	
	public static String getDadosColaborador() {
		StringBuilder strb = new StringBuilder();
		strb.append(" SELECT");
		strb.append(" cc.GESTOR_COLABORADOR,");
		strb.append(" cc.NOME_COLABORADOR,");
		strb.append(" cc.UNIDADE_FUNCIONAL,");
		strb.append(" cc.CARGO_FUNCAO,");
		strb.append(" cc.TOTAL_RV_CARGO,");
		strb.append(" cc.SALARIO_ATUAL,");
		strb.append(" cc.MESES_TRABALHADO,");
		strb.append(" cc.LOGIN_GESTOR,");
		strb.append(" cc.AVALIACAO_180");
		strb.append(" FROM cadastro_colaborador cc");
		strb.append(" INNER JOIN colaborador_indicador ci ON ci.nome_colaborador = cc.nome_colaborador");
		strb.append(" ORDER BY GESTOR_COLABORADOR");
		return strb.toString();
	}
	
	public static String getIndicadores(String nomeColaborador) {
		StringBuilder strb = new StringBuilder();
		strb.append(" SELECT");
		strb.append(" ci.NOME_COLABORADOR,");
		strb.append(" ci.TIPO_INDICADOR,");
		strb.append(" ci.NOME_INDICADOR,");
		strb.append(" ci.DESCRICAO_INDICADOR,");
		strb.append(" ci.FIRMADO,");
		strb.append(" ci.PESO");
		strb.append(" FROM colaborador_indicador ci");
		strb.append(" INNER JOIN cadastro_colaborador cc ON cc.nome_colaborador = ci.nome_colaborador");
		strb.append(" WHERE ci.NOME_COLABORADOR = '" +nomeColaborador+ "'");
		strb.append(" ORDER BY TIPO_INDICADOR");
		return strb.toString();
	}
	
	// --------------------------------------------------------------------------------
	//--------------------Select info Processo -----------------
	
	public static String getInfoProcesso(String cod_processo) {
		StringBuilder strb = new StringBuilder();
		strb.append(" SELECT");
		strb.append(" LISTA");
		strb.append(" FROM f_lib_aval_dese");
		strb.append(" WHERE cod_processo_f = " + cod_processo);
		strb.append(" AND cod_etapa_f = 2");
		return strb.toString();
	}
	
	// --------------------------------------------------------------------------------
	//--------------------Select Email -----------------
	
	public static String getEmailGestor(String cod_processo) {
		StringBuilder strb = new StringBuilder();
		strb.append(" SELECT DISTINCT");
		strb.append(" us.des_email");
		strb.append(" FROM usuario us");
		strb.append(" INNER JOIN cadastro_colaborador cc ON cc.login_gestor = us.des_login");
		return strb.toString();
	}
	
}
