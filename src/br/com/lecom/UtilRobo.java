package br.com.lecom;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import com.lecom.workflow.cadastros.rotas.AbreProcesso;
import com.lecom.workflow.cadastros.rotas.AprovaProcesso;
import com.lecom.workflow.cadastros.rotas.LoginAutenticacao;
import com.lecom.workflow.cadastros.rotas.util.DadosLogin;
import com.lecom.workflow.cadastros.rotas.util.DadosProcesso;
import com.lecom.workflow.cadastros.rotas.util.DadosProcessoAbertura;

public class UtilRobo {

//	Map<String, String> campos = new HashMap<>();
//	campos.put("ID_CAMPO", "yyyy-MM-dd");

//	LinkedList<Map<String, Object>> camposGrid = new LinkedList<>();
//	Map<String, String> campos = new HashMap<>();
//	campos.put("ID_GRID", null);
//	campos.put("ID_CAMPO", null);
//	camposGrid.add(campos);

	/**
	 * abre um processo;
	 * 
	 * @param map        map com as chaves do UtilRobo.properties;
	 * @param mapAvaliador     map com os campos do formulario que devem ser preenchidos;
	 * @param gridAvaliado list com as grids do formulario que devem ser preenchidas;
	 * @return c�digo do processo aberto;
	 * @throws Exception
	 */
	public static Integer abreProcesso(Map<String, String> map, Map<String, String> mapAvaliador,
			List<Map<String, Object>> gridAvaliado) throws Exception {
		DadosLogin dadosLogin = new DadosLogin(map.get("usrRobo"), map.get("pwdRobo"), false);
		LoginAutenticacao loginAuthentication = new LoginAutenticacao(map.get("urlBpm") + "/sso", dadosLogin);
		String token = loginAuthentication.getToken();
		DadosProcesso dadosProcesso = new DadosProcesso("P");

		// preenche os campos do formulario;
		// ----------------------------------------------------------------------------------------
		if (mapAvaliador != null)
			dadosProcesso.geraPadroes(mapAvaliador);

		// preenche as grids do formulario;
		// ----------------------------------------------------------------------------------------
		if (gridAvaliado != null) {
			TreeSet<String> gridsTratadas = new TreeSet<>();

			// percorre list de grids;
			for (Map<String, Object> map2 : gridAvaliado) {
				String gridCorrente = Util.obj2String(map2.get("nomeGrid"));

				// se a grid corrente n�o foi tratada...
				if (!gridsTratadas.contains(gridCorrente)) {

					// recupera registros da grid corrente;
					List<Map<String, Object>> list = new LinkedList<>();
					for (Map<String, Object> map3 : gridAvaliado) {
						if (gridCorrente.equals(Util.obj2String(map3.get("nomeGrid")))) {
							Map<String, Object> map4 = new HashMap<>();
							map4.putAll(map3);
							map4.remove("nomeGrid");
							list.add(map4);
						}
					}

					// preenche grid corrente;
					dadosProcesso.geraValoresGrid(gridCorrente, list);
				}

				// insere grid corrente na cole��o de grids tratadas;
				gridsTratadas.add(gridCorrente);
			}
		}

		AbreProcesso abreProcesso = new AbreProcesso(map.get("urlBpm") + "/bpm", token, map.get("codForm"),
				map.get("codVersao"), (map.get("modoTeste").equalsIgnoreCase("S")) ? "true" : "false",
				map.get("codRobo"), null);
		DadosProcessoAbertura dadosProcessoAbertura = abreProcesso.getAbreProcesso();
		AprovaProcesso aprovaProcesso = new AprovaProcesso(map.get("urlBpm") + "/bpm", token, dadosProcessoAbertura,
				dadosProcesso, (map.get("modoTeste").equalsIgnoreCase("S")) ? "true" : "false", map.get("codRobo"));
		aprovaProcesso.aprovaProcesso();
		return Util.obj2Integer(dadosProcessoAbertura.getProcessInstanceId());
	}

	/**
	 * aprova ou rejeita uma etapa;
	 * 
	 * @param map map com as chaves do UtilRobo.getRoboMap();
	 * @throws Exception
	 */
	public static void executaEtapa(Map<String, String> map) throws Exception {
		DadosLogin dadosLogin = new DadosLogin(map.get("usrRobo"), map.get("pwdRobo"), false);
		LoginAutenticacao loginAuthentication = new LoginAutenticacao(map.get("urlBpm") + "/sso", dadosLogin);
		String token = loginAuthentication.getToken();
		DadosProcessoAbertura dadosProcUtil = new DadosProcessoAbertura();
		dadosProcUtil.setProcessInstanceId(map.get("codProcesso"));
		dadosProcUtil.setCurrentActivityInstanceId(map.get("codEtapa"));
		dadosProcUtil.setCurrentCycle(map.get("codCiclo"));
		dadosProcUtil.setModoTeste(map.get("modoTeste").equalsIgnoreCase("S") ? "true" : "false");
		DadosProcesso dadosProcesso = new DadosProcesso(map.get("acao"));
		// dadosProcesso.geraPadroes(null);
		AprovaProcesso aprovaProcesso = new AprovaProcesso(map.get("urlBpm") + "/bpm", token, dadosProcUtil,
				dadosProcesso, map.get("modoTeste").equalsIgnoreCase("S") ? "true" : "false", map.get("codRobo"));
		aprovaProcesso.aprovaProcesso();
	}

	/**
	 * 
	 * @param codForm  c�digo do fluxo;
	 * @param codEtapa c�digo da etapa;
	 * @return query que busca processos pendentes;
	 */
	public static String getProcesso(String codForm, String codEtapa) {
		StringBuilder strb = new StringBuilder();
		strb.append(" SELECT");
		strb.append(" p.COD_PROCESSO,");
		strb.append(" p.COD_ETAPA_ATUAL,");
		strb.append(" p.COD_CICLO_ATUAL");
		strb.append(" FROM processo p");
		strb.append(" LEFT JOIN processo_etapa pe");
		strb.append(" ON pe.cod_processo = p.cod_processo");
		strb.append(" AND pe.cod_etapa = p.cod_etapa_atual");
		strb.append(" AND pe.cod_ciclo = p.cod_ciclo_atual");
		strb.append(" WHERE");
		strb.append(" p.cod_form = " + codForm);
		strb.append(" AND p.cod_etapa_atual = " + codEtapa);
		strb.append(" AND pe.ide_status = 'A'");
		return strb.toString();
	}

	/**
	 * 
	 * @param codForm  c�digo do fluxo;
	 * @param codEtapa c�digo da etapa;
	 * @return query que busca processos em paralelo pendentes;
	 */
	public static String getProcessoParalelo(String codForm, String codEtapa) {
		StringBuilder strb = new StringBuilder();
		strb.append(" SELECT");
		strb.append(" pp.COD_PROCESSO,");
		strb.append(" pp.COD_ETAPA_ATUAL,");
		strb.append(" pp.COD_CICLO_ATUAL");
		strb.append(" FROM processo_paralelo pp");
		strb.append(" LEFT JOIN processo p");
		strb.append(" ON p.cod_processo = pp.cod_processo");
		strb.append(" AND p.cod_ciclo_atual = pp.cod_ciclo_atual");
		strb.append(" WHERE");
		strb.append(" p.cod_form = " + codForm);
		strb.append(" AND pp.cod_etapa_atual = " + codEtapa);
		strb.append(" AND pp.ide_finalizado = 'A'");
		return strb.toString();
	}

//	roboMap.replace("codProcesso", rs.getString("COD_PROCESSO"));
//	roboMap.replace("codEtapa", rs.getString("COD_ETAPA_ATUAL"));
//	roboMap.replace("codCiclo", rs.getString("COD_CICLO_ATUAL"));

	/**
	 * 
	 * @return map com as chaves do UtilRobo.properties;
	 */
	public static Map<String, String> getRoboMap() {
		Map<String, String> roboMap = new HashMap<>();
		try {
			roboMap.put("usrRobo", Util.getContentProperties(true, "UtilRobo.properties", "usrRobo"));
			roboMap.put("pwdRobo", Util.getContentProperties(true, "UtilRobo.properties", "pwdRobo"));
			roboMap.put("codRobo", Util.getContentProperties(true, "UtilRobo.properties", "codRobo"));
			roboMap.put("urlBpm", Util.getContentProperties(true, "UtilRobo.properties", "urlBpm"));
			roboMap.put("codForm", null);
			roboMap.put("codVersao", Util.getContentProperties(true, "UtilRobo.properties", "codVersao"));
			roboMap.put("modoTeste", Util.getContentProperties(true, "UtilRobo.properties", "modoTeste"));
			roboMap.put("codProcesso", null);
			roboMap.put("codEtapa", null);
			roboMap.put("codCiclo", null);
			roboMap.put("acao", "P");
		} catch (Exception e) {
			// TODO: handle exception
		}
		return roboMap;
	}
	
	//roboMap.put("codForm_Liberacao", Util.getContentProperties(true, "UtilRobo.properties", "codForm_Liberacao"));
	//roboMap.put("codForm_180", Util.getContentProperties(true, "UtilRobo.properties", "codForm_180"));
	//roboMap.put("codForm_Indicadores", Util.getContentProperties(true, "UtilRobo.properties", "codForm_Indicadores"));
	//roboMap.put("codForm_Desempenho", Util.getContentProperties(true, "UtilRobo.properties", "codForm_Desempenho"));

}

// public static Integer abreProcessoOld(Map<String, String> map) {
// Integer numProcesso = null;
// GenericWSVO genericWSVO = new GenericWSVO();
// genericWSVO.setLoginUsuario(map.get("usrRobo"));
// genericWSVO.setCodForm(Util.obj2Integer(map.get("codForm")));
// String sRetornoWS = RBOpenWebServices.getInstance().executeWebServices(genericWSVO,
// 		RBOpenWebServices.ABRE_PROCESSO);
// String rRetorno[] = sRetornoWS.split("\\|");
// if (rRetorno[0].equals("0"))
// 	numProcesso = Integer.parseInt(rRetorno[1]);
// return numProcesso;
// }

// public static void executaEtapaOld(Map<String, String> map) throws Exception {
// GenericWSVO genericWSVO = new GenericWSVO();
// genericWSVO.setLoginUsuario(map.get("usrRobo"));
// genericWSVO.setSenhaUsuario(map.get("pwdRobo"));
// genericWSVO.setCodProcesso(Util.obj2Integer(map.get("codProcesso")));
// genericWSVO.setCodEtapa(Util.obj2Integer(map.get("codEtapa")));
// genericWSVO.setCodCiclo(Util.obj2Integer(map.get("codCiclo")));
// genericWSVO.setAcao(map.get("acao"));
// RBOpenWebServices.getInstance().executeWebServices(genericWSVO, RBOpenWebServices.EXECUTA_ETAPA_PROCESSO);
// }